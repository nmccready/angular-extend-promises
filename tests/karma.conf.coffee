module.exports = (config) ->
  config.set
    basePath: '..'

    frameworks: [
      'mocha'
      'chai'
      'chai-as-promised'
      'sinon-chai'
    ]

    reporters: ['mocha']
    captureTimeout: 10000

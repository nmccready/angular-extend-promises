describe('Promise.each', function() {
  var newq;
  var $rootScope;
  var $timeout;

  // #direct port from bluebird.each specs
  function promised (val, mill) {
    mill = (typeof(mill) === 'undefined' || mill === null) ? 1 : mill;
    //  using $timeout for delay as specs hate setTimeout in angular
    //  underlying code for delay is setTimeout due to circular dependency on $timeout to $q
    return newq(function(resolve){
      $timeout(function(){
        resolve(val);
      }, mill);
    });
  }

  beforeEach(module('angular-extend-promises'));
  beforeEach(inject(function($q, _$rootScope_) {
    newq = $q;
    $rootScope = _$rootScope_;
  }));

  function add1(val) {
    return val + 1;
  }

  it('should be able to iterate over a value returned by a promise', function(done) {
    expect(newq.resolve([1, 3]).each(add1))
      .to.eventually.deep.equal([1, 3])
      .notify(done)
    ;

    $rootScope.$digest();
  });

  it('should be able to iterate over a value returned by newq.each', function(done) {
    expect(newq.each([1, 3], add1).each(add1))
      .to.eventually.deep.equal([1, 3])
      .notify(done)
    ;

    $rootScope.$digest();
  });

  it('should be able to iterate over a value returned by another promise.each', function(done) {
    expect(newq.resolve([1, 3]).each(add1).each(add1))
      .to.eventually.deep.equal([1, 3])
      .notify(done)
    ;

    $rootScope.$digest();
  });

  describe('series', function(done) {
    it('should flow promises down each synchronously after a newq.each', function(done) {
      newq.each([promised(1,2), 2], function(val) {
        console.log("newq.each")
        console.log(val);
        return val;
      })
      .each(add1)
      .each(function(val, i) {
        expect(val).to.be.equal(i + 1);
        done();
      });

      $rootScope.$digest();
    });

    it('should flow promises down each synchronously after a promise.each', function(done) {
      newq.resolve([promised(1,2), 2])
        .each(add1)
        .each(function(val, i) {
          expect(val).to.be.equal(i + 1);
          done();
        })
      ;

      $rootScope.$digest();
    });
  });

  describe('non-series', function(done) {
    //newq.defer().promise non-resolving promises
    it('should flow promises down each asynchronously after a newq.each', function(done) {
      newq.each([newq.defer().promise, 1], function(val) {
        return val;
      }, {isSeries:false})
        .each(add1)
        .each(function(val) {
          expect(val).to.be.equal(1);
          done();
        })
      ;

      $rootScope.$digest();
    });

    it('should flow promises down each asynchronously after a promise.each', function(done) {
      newq.resolve([newq.defer().promise, 2])
        .each(add1, null, {isSeries:false})
        .each(function(val) {
          expect(val).to.be.equal(2);
          done();
        }, {isSeries:false})
      ;

      $rootScope.$digest();
    });
  });

  it('should call the callback with index & array length', function() {
    var spy = sinon.spy();

    newq.resolve([21, 42]).each(spy);

    $rootScope.$digest();

    expect(spy)
      .to.have.been.calledTwice
      .and.to.have.been.calledWithExactly(21, 0, 2)
      .and.to.have.been.calledWithExactly(42, 1, 2)
    ;
  });
});

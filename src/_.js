'use strict';

/* global _ */

try {
  module.exports = require('lodash');
}
catch (e) {
  module.exports = _;
}

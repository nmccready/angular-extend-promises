'use strict';

var each = require('./each');

module.exports = function (collection, cb) {
  return each(collection, cb);
};

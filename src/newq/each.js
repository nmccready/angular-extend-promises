'use strict';

var _ = require('../_');
var reduce = require('./reduce');
var walkCollection = require('./walkCollection');
var map = require('./map');

module.exports = function(collection, cb, options) {
  options = options || {};
  var isSeries = true;

  if (typeof(options.isSeries) !== 'undefined' || options.isSeries !== null)
    isSeries = options.isSeries;
    delete options.isSeries;

  if (isSeries)
    return reduce(collection, function() {
        var args = _.toArray(arguments);
        var acc =  args.shift();
        return cb.apply(this, args);
      }
    , null)
    .return(map(collection, function(v) { return v;}));

  return walkCollection('tap')(collection, cb, options);
};
